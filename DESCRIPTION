Package: STRINGdb
Type: Package
Title: STRINGdb - Protein-Protein Interaction Networks and Functional
        Enrichment Analysis
Version: 2.18.0
Date: 2024-06-05
Author: Andrea Franceschini <andrea.franceschini@isb-sib.ch>
Maintainer: Damian Szklarczyk <damian.szklarczyk@imls.uzh.ch>
Description: The STRINGdb package provides a R interface to the STRING protein-protein interactions database (https://string-db.org).
License: GPL-2
Depends: R (>= 2.14.0)
Imports: png, sqldf, plyr, igraph, httr, methods, RColorBrewer, gplots,
        hash, plotrix
Suggests: RUnit, BiocGenerics
biocViews: Network
git_url: https://git.bioconductor.org/packages/STRINGdb
git_branch: RELEASE_3_20
git_last_commit: 4005322
git_last_commit_date: 2024-10-29
Repository: Bioconductor 3.20
Date/Publication: 2024-10-29
NeedsCompilation: no
Packaged: 2024-10-30 03:09:16 UTC; biocbuild
